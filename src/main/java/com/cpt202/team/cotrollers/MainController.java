package com.cpt202.team.cotrollers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    // http://localhost:8001/
    // Everyone

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("username", "Thomas");
        return "home";
    }
}
