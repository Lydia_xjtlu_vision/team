package com.cpt202.team.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.cpt202.team.models.User;

public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
}
